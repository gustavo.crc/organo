import './CampoTexto.css'

const CampoTexto = (props) => {

  // Define uma função chamada 'aoDigitado' que será chamada quando o conteúdo do campo de texto mudar.
  const aoDigitado = (evento) => {
    // Chama a função 'aoAlterado' fornecida através das props, passando o valor digitado como argumento.
    props.aoAlterado(evento.target.value)
  }

  return (
    <div className="campo-texto">
      <label>{props.label}</label>
      <input value={props.valor} onChange={aoDigitado} required={props.obrigatorio} placeholder={props.placeholder} />
    </div>
  )
}

export default CampoTexto