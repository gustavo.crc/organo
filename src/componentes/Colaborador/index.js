import './Colaborador.css'

// Nesse componentes não estou utilizando o props, este é um padrão de desestruturação,
// onde olho pro componente e sei que precisa ser passado os parametros: nome, imagem, cargo
const Colaborador = ({ nome, imagem, cargo, corDeFundo }) => {
  return (<div className='colaborador'>
      <div className='cabecalho' style={{backgroundColor: corDeFundo}}>
          <img src={imagem} alt={nome}/>
      </div>
      <div className='rodape'>
          <h4>{nome}</h4>
          <h5>{cargo}</h5>
      </div>
  </div>)
}

export default Colaborador