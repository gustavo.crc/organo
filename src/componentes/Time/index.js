import Colaborador from '../Colaborador'
import './Time.css'

const Time = (props) => {
  const css = { backgroundColor: props.corSecundaria }

  // A expressão condicional props.colaboradores.length > 0 && ... permite renderizar o JSX somente se a condição for verdadeira,
  // evitando a renderização desnecessária se não houver colaboradores. Se for verdadeira o JSX após o && será renderizado
  return (
    props.colaboradores.length > 0 && <section className='time' style={css}>
      <h3 style={{ borderColor: props.corPrimaria }}>{props.nome}</h3>
      <div className='colaboradores'>
        {props.colaboradores.map(colaborador => <Colaborador corDeFundo={props.corPrimaria} key={colaborador.nome} nome={colaborador.nome} cargo={colaborador.cargo} imagem={colaborador.imagem} />)}
      </div>
    </section>
  )
}

export default Time