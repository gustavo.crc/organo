import './Banner.css'

function Banner() {
  // JSX
  return (
    // Usar className pra não conflitar com método nativo do JS
    <header className='banner'>
      <img src="/imagens/banner.png" alt="Banner principal da aplicação"/>
    </header>
  )
}

export default Banner