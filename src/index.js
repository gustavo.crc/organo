import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // StrictMode é algo que ajuda no ambiente de dev, pois gera logs claros de erros.
  <React.StrictMode>
    <App />
  </React.StrictMode>
);